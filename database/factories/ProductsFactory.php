<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $images["first"] = $this->faker->text(50);
        $images["second"] = $this->faker->text(50);
        $images["third"] = $this->faker->text(50);

        return [
            'name' => $this->faker->firstName(),
            'price' => random_int(100, 999),
            'shortdesc' => $this->faker->text(254),
            'longdesc' => $this->faker->text(1000),
            'thumbnail' => $this->faker->text(100),
            'images' => json_encode($images)
        ];
    }
}
