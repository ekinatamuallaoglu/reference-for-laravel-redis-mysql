<?php

use App\Http\Controllers\ProductsController;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Cache::has('User')) {
        $data = Cache::get('User');
        return response()->json($data, 200);
    }

    $data = User::all();

    foreach ($data as $item) {
        $item->status = true;
    }

    Cache::put('User', $data, 600);

    return response()->json($data, 200);
});

Route::apiResource('products', ProductsController::class);
